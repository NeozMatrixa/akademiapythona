"""Forms"""
from django.forms import ModelForm
from .models import Category


class CreateCategoryForm(ModelForm):
    """Create category form"""

    class Meta:
        """Meta"""
        model = Category
        fields = ['name', 'order_number']
