from django.views.generic import ListView, CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from .forms import CreateCategoryForm
from .models import Category


class IndexView(ListView):
    """ Index view """
    model = Category
    template_name = "index.html"


index_view = IndexView.as_view()


class AddCategoryView(CreateView):
    """Add categorry view"""
    model = Category
    form_class = CreateCategoryForm
    template_name = "create_category.html"
    success_url = reverse_lazy('core:index')


create_category_view = AddCategoryView.as_view()

class DeleteCategoryView(DeleteView):
    """Delete category view"""
    model = Category
    template_name = "delete_category.html"
    success_url = reverse_lazy('core:index')

delete_category_view = DeleteCategoryView.as_view()

class UpdateCategoryView(UpdateView):
    model = Category
    form_class = CreateCategoryForm
    template_name = "create_category.html"
    success_url = reverse_lazy('core:index')

update_category_view = UpdateCategoryView.as_view()
